# README #

Sample application for Sergio Flores job interview.

Please note the architecture and reuse of components in a parent child relationship or as a finite state machine.

The app was done using the following tools:

● Typescript
● Angular (Latest version)
● SCSS
● Angular Router
● Ngrx
● HTML5
● Angular Material
● Twitter Bootstrap