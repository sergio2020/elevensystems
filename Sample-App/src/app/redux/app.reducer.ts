import { AppState, AppStatus } from './app.state';
import { createReducer, on } from '@ngrx/store';
import { loadnewcompetitiondetails, newcompetitionsdetailsrendered, renderingcompetitiondetails, renderingdashboard, renderingfavorites } from './app.actions';

const initialState: AppState = { status: AppStatus.LoadingDashboard, standings: null };

const _appReducer = createReducer(
    initialState,
    on(renderingdashboard, (state) => state = {status: AppStatus.LoadingDashboard, standings: null}),
    on(renderingcompetitiondetails, (state) => state = {status: AppStatus.LoadingCompetitionStandings, standings: null}),
    on(loadnewcompetitiondetails, (state) => state = {status: AppStatus.ReloadingCompetitionStandings, standings: null}),
    on(newcompetitionsdetailsrendered, (state) => state = {status: AppStatus.LoadedCompetitionStandings, standings: null}),
    on(renderingfavorites, (state) => state = {status: AppStatus.LoadingFavorites, standings: null})
  );

  export function appReducer(state, action) {
    return _appReducer(state, action);
  }