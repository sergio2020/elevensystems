import { createAction, props } from '@ngrx/store';
import { CompetitionStanding } from '../models/competition-standing-model';
import { AppStatus } from './app.state';
  
  export const renderingdashboard = createAction('[Dashboard Component] RENDERINGDASHBOARD');
  export const renderingcompetitiondetails = createAction('[CompetitionDetails Component] RENDERINGCOMPETITIONDETAILS');
  export const loadnewcompetitiondetails = createAction('LOADNEWCOMPETITIONDETAILS', props<{standings: CompetitionStanding[]}>());
  export const newcompetitionsdetailsrendered = createAction('NEWCOMPETITIONDETAILSRENDERED');
  export const renderingfavorites = createAction('[Favorites Component] RENDERINGFAVORITES');