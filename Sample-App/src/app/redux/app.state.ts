import { CompetitionStanding } from '../models/competition-standing-model';

export enum AppStatus{
  LoadingDashboard = 0,
  LoadingCompetitionStandings = 1,
  LoadingFavorites = 2,
  ReloadingCompetitionStandings = 3,
  LoadedCompetitionStandings = 4
}

export interface AppState {
  status: AppStatus;
  standings: CompetitionStanding[];
};