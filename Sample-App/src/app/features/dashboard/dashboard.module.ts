import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { FootballCompetitionsListComponent } from './competition/football-competitions-list/football-competitions-list.component';
import { MatDialogModule, MatSortModule, MatTableModule } from '@angular/material';
import { SharedAssetsModule } from '../shared-assets/shared-assets.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    DashboardComponent, 
    FootballCompetitionsListComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedAssetsModule,
    MatTableModule,
    MatDialogModule,
    MatSortModule
  ],
  exports:[
    DashboardComponent,
    FootballCompetitionsListComponent
  ],
})
export class DashboardModule { }