import { Component, OnInit } from '@angular/core';
import { CompetitionApiService } from 'src/app/middleware/api-services/competition-api.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { Competition, CompetitionApiContainer, CompetitionsApiContainer } from '../../../../models/competition-model';
import { forkJoin } from 'rxjs';
import { Sort } from '@angular/material';
import { ApplicationAssetsService } from 'src/app/middleware/local-services/application-assets.service';
import { CompetitionStandingsApiContainer } from 'src/app/models/competition-standing-model';

@Component({
  selector: 'app-football-competitions-list',
  templateUrl: './football-competitions-list.component.html',
  styleUrls: ['./football-competitions-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class FootballCompetitionsListComponent implements OnInit {
  dataSource:Competition[] = new Array<Competition>();
  displayedColumns: string[] = ['area','name','currentMatchDay', 'startDate', 'endDate', 'moreInfo'];
  expandedElement: Competition = null;
  loadingData:boolean = true;

  private competitionsBuffer:Competition[] = new Array<Competition>();

  constructor(public applicationAssetsService:ApplicationAssetsService, private competitionApiService:CompetitionApiService) { }

  ngOnInit() {
    this.loadCompetitions();
  }

  private loadCompetitions(){
    this.competitionApiService.getAllCompetitions().subscribe(competitionsCall => {
      this.loadCompetitionsCallback(competitionsCall);
    })
  }

  private loadCompetitionsCallback(competitionsCall:CompetitionsApiContainer){
    this.competitionsBuffer = competitionsCall.competitions.filter(competition => competition.plan == "TIER_ONE");
    this.dataSource = this.competitionsBuffer;
    this.loadingData = false;
  }

  loadTeamsPerCompetition(competitionId:number){
    forkJoin(
      this.competitionApiService.getCompetitionTeamsCurrentSeason(competitionId),
      this.competitionApiService.getCompetitionStandingsCurrentSeason(competitionId)
    )
    .subscribe(([competitionTeamCall,competitionStandingsCall]) => {
      this.loadTeamsPerCompetitionCallback(competitionTeamCall,competitionStandingsCall);
    })
  }

  private loadTeamsPerCompetitionCallback(competitionTeamCall:CompetitionApiContainer, competitionStandingsCall:CompetitionStandingsApiContainer){
    let targetCompetition = this.competitionsBuffer.find(competition => competition.id == competitionTeamCall.competition.id);
      targetCompetition.teams = competitionTeamCall.teams;
      targetCompetition.standings = competitionStandingsCall.standings;      
  }

  onClickedRow(competition:Competition){
    competition.isExpanded = !competition.isExpanded;
    if(competition.isExpanded && competition.teams == null)
    {
      this.loadTeamsPerCompetition(competition.id);
    }    
  }

  sortData(sort: Sort) {
    const data = this.competitionsBuffer.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource = data;
      return;
    }
  
    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'area': return this.applicationAssetsService.compare(a.area.name, b.area.name, isAsc);
        case 'name': return this.applicationAssetsService.compare(a.name, b.name, isAsc);
        case 'currentMatchDay': return this.applicationAssetsService.compare(a.currentSeason.currentMatchday, b.currentSeason.currentMatchday, isAsc);      
        case 'startDate': return this.applicationAssetsService.compareDate(a.currentSeason.startDate, b.currentSeason.startDate, isAsc);
        case 'endDate': return this.applicationAssetsService.compareDate(a.currentSeason.endDate, b.currentSeason.endDate, isAsc);
        default: return 0;
      }
      });
    }
}
