import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FootballCompetitionsListComponent } from './football-competitions-list.component';

describe('FootballCompetitionsListComponent', () => {
  let component: FootballCompetitionsListComponent;
  let fixture: ComponentFixture<FootballCompetitionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FootballCompetitionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FootballCompetitionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
