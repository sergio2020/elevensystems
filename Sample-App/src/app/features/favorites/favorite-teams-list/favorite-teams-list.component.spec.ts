import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteTeamsListComponent } from './favorite-teams-list.component';

describe('FavoriteTeamsListComponent', () => {
  let component: FavoriteTeamsListComponent;
  let fixture: ComponentFixture<FavoriteTeamsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteTeamsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteTeamsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
