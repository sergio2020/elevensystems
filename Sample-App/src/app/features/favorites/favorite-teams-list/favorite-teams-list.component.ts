import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatTableDataSource } from '@angular/material/table';
import { TeamMatchesDialogComponent } from '../../shared-assets/match/team-matches-dialog/team-matches-dialog.component';
import { Team } from '../../../models/team-model';
import * as AppActions from 'src/app/redux/app.actions';
import { AppState } from 'src/app/redux/app.state';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-favorite-teams-list',
  templateUrl: './favorite-teams-list.component.html',
  styleUrls: ['./favorite-teams-list.component.scss']
})
export class FavoriteTeamsListComponent implements OnInit {
  isValidList:boolean = true;
  dataSource = new MatTableDataSource();
  displayedColumns: string[] = ['area','name','competitionDetails'];
  
  constructor(private store: Store<AppState>, public dialog: MatDialog) { }

  ngOnInit() {
    this.loadTeams();
    this.store.dispatch(AppActions.renderingfavorites());
  }

  private loadTeams(){
    if(localStorage.getItem('favoriteTeams') == null)
    {
      this.isValidList = false;
    }
    else{
      let favoriteTeamsList:Team[] = JSON.parse(localStorage.getItem('favoriteTeams'));
      this.dataSource = new MatTableDataSource(favoriteTeamsList.sort(function(a, b){return (a.name < b.name ? -1 : 1)}));
    }    
  }

  openCompetitionDetails(team:Team){
    this.dialog.open(TeamMatchesDialogComponent, {
      data: team,
      width: '600px'
    });
  }
}
