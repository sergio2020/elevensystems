import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FavoritesRoutingModule } from './favorites-routing.module';
import { MatTableModule } from '@angular/material';
import { SharedAssetsModule } from '../shared-assets/shared-assets.module';
import { FavoriteTeamsListComponent } from './favorite-teams-list/favorite-teams-list.component';


@NgModule({
  declarations: [
    FavoriteTeamsListComponent
  ],
  imports: [
    CommonModule,
    FavoritesRoutingModule,
    SharedAssetsModule,
    MatTableModule
  ],
  exports:[
    FavoriteTeamsListComponent
  ],
})
export class FavoritesModule { }