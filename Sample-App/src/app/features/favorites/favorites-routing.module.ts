import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FavoriteTeamsListComponent } from './favorite-teams-list/favorite-teams-list.component';

const routes: Routes = [
  {
    path: '',
    component: FavoriteTeamsListComponent
  },
  {
    path: 'favoriteteams',
    component: FavoriteTeamsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FavoritesRoutingModule { }