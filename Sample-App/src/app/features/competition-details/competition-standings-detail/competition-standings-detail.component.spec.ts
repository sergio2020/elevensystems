import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitionStandingsDetailComponent } from './competition-standings-detail.component';

describe('CompetitionStandingsDetailComponent', () => {
  let component: CompetitionStandingsDetailComponent;
  let fixture: ComponentFixture<CompetitionStandingsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompetitionStandingsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitionStandingsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
