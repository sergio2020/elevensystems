import { Component, OnInit } from '@angular/core';
import { CompetitionApiService } from 'src/app/middleware/api-services/competition-api.service';
import { Competition, CompetitionsApiContainer } from 'src/app/models/competition-model';
import { CompetitionStanding } from 'src/app/models/competition-standing-model';
import * as AppActions from 'src/app/redux/app.actions';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/redux/app.state';

@Component({
  selector: 'app-competition-standings-detail',
  templateUrl: './competition-standings-detail.component.html',
  styleUrls: ['./competition-standings-detail.component.scss']
})
export class CompetitionStandingsDetailComponent implements OnInit {
  availableCompetitions:Competition[] = new Array<Competition>();
  selectedCompetition:Competition = new Competition();
  loadingData:boolean = true;

  private competitionsBuffer:Competition[] = new Array<Competition>();
  
  constructor(private competitionApiService:CompetitionApiService,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.loadCompetitions();
    this.store.dispatch(AppActions.renderingcompetitiondetails());
  }

  private loadCompetitions(){
    this.competitionApiService.getAllCompetitions().subscribe(competitionsCall => {
      this.loadCompetitionsCallback(competitionsCall);
    })
  }

  private loadCompetitionsCallback(competitionsCall:CompetitionsApiContainer){
    this.competitionsBuffer = competitionsCall.competitions.filter(competition => competition.plan == "TIER_ONE");
    this.availableCompetitions = this.competitionsBuffer;
    this.loadingData = false;
  }

  onCompetitionSelected(){
    this.competitionApiService.getCompetitionStandingsCurrentSeason(this.selectedCompetition.id).subscribe(competitionCall => {
      this.onCompetitionSelectedCallback(competitionCall.standings)
    } )
  }

  onCompetitionSelectedCallback(standings:CompetitionStanding[]){
    this.store.dispatch(AppActions.loadnewcompetitiondetails({standings: standings}));
  }
}
