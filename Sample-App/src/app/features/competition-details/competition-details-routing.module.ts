import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompetitionStandingsDetailComponent } from './competition-standings-detail/competition-standings-detail.component';

const routes: Routes = [
  {
    path: '',
    component: CompetitionStandingsDetailComponent
  },
  {
    path: 'competitionstandings',
    component: CompetitionStandingsDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompetitionDetailsRoutingModule { }