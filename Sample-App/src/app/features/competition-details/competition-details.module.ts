import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSelectModule, MatSortModule, MatTableModule } from '@angular/material';
import { SharedAssetsModule } from '../shared-assets/shared-assets.module';
import { CompetitionDetailsRoutingModule } from './competition-details-routing.module';
import { CompetitionStandingsDetailComponent } from './competition-standings-detail/competition-standings-detail.component';


@NgModule({
  declarations: [
    CompetitionStandingsDetailComponent
  ],
  imports: [
    CommonModule,
    CompetitionDetailsRoutingModule,
    SharedAssetsModule,
    MatTableModule,
    MatSortModule,
    MatSelectModule
  ],
  exports:[
    CompetitionStandingsDetailComponent
  ],
})
export class CompetitionDetailsModule { }