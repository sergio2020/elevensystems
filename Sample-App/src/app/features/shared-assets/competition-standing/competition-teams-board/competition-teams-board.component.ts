import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {MatSort, Sort} from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { CompetitionStanding, Standing } from '../../../../models/competition-standing-model';
import { ApplicationAssetsService } from 'src/app/middleware/local-services/application-assets.service';
import { Team } from '../../../../models/team-model';
import { TeamDetailDialogComponent } from '../../team/team-detail-dialog/team-detail-dialog.component';
import { AppState } from 'src/app/redux/app.state';
import * as AppActions from 'src/app/redux/app.actions';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-competition-teams-board',
  templateUrl: './competition-teams-board.component.html',
  styleUrls: ['./competition-teams-board.component.scss']
})
export class CompetitionTeamsBoardComponent implements OnInit {
  @Input('standings') standings: CompetitionStanding[];

  @ViewChild(MatSort,{static: false}) sort: MatSort;

  dataSource:Standing[] = new Array<Standing>();
  displayedColumns: string[] = ['name','crest','position','playedGames','wonGames','draw','lost','points','goalsInFavor','goalsAgainst','goalDifference'];
  loadingData:boolean = true;

  private masterStandingList:Standing[] = new Array<Standing>();
  private onLoadNewCompetitionsDetailsSubscription: Subscription; 
  private onCompetitionsDetailsRenderedSubscription: Subscription; 

  constructor(private store: Store<AppState>, 
    public applicationAssetsService:ApplicationAssetsService,
    public dialog: MatDialog,
    private updates$: Actions) { }

  ngOnInit() {
    this.standingsSubscriptionStatesEngine();
    this.loadStandigsTable();    
  }

  private loadStandigsTable(){
    if(this.standings != null)
    {
      let competitionStanding = this.standings.find(standings => standings.type == "TOTAL").table;
      this.dataSource = competitionStanding.slice().sort(standing => standing.position); 
      this.masterStandingList = competitionStanding.slice().sort(standing => standing.position);
    }
    this.store.dispatch(AppActions.newcompetitionsdetailsrendered());
  }

  private standingsSubscriptionStatesEngine(){
    this.onCompetitionsDetailsRenderedSubscription = this.updates$
        .pipe(
          ofType('NEWCOMPETITIONDETAILSRENDERED'))
        .subscribe((data) => {
          this.onLoadedCompetitionStandings();
        });
      this.onLoadNewCompetitionsDetailsSubscription = this.updates$
        .pipe(
          ofType('LOADNEWCOMPETITIONDETAILS'))
        .subscribe((data:AppState) => {
          this.onReloadingCompetitionStandings(data.standings);
        });
  }

  private onLoadedCompetitionStandings(){
    this.loadingData = false;
  }

  private onReloadingCompetitionStandings(standings:CompetitionStanding[]){
    this.standings = standings;
    this.loadStandigsTable();
  }

  showTeamDetails(team: Team){
    this.dialog.open(TeamDetailDialogComponent, {
     data: team,
     width: '600px'
   });     
 }

 sortData(sort: Sort) {
  const data = this.masterStandingList.slice();
  if (!sort.active || sort.direction === '') {
    this.dataSource = data;
    return;
  }

  this.dataSource = data.sort((a, b) => {
    const isAsc = sort.direction === 'asc';
    switch (sort.active) {
      case 'name': return this.applicationAssetsService.compare(a.team.name, b.team.name, isAsc);
      case 'position': return this.applicationAssetsService.compare(a.position, b.position, isAsc);
      case 'playedGames': return this.applicationAssetsService.compare(a.playedGames, b.playedGames, isAsc);      
      case 'points': return this.applicationAssetsService.compare(a.points, b.points, isAsc);
      case 'wonGames': return this.applicationAssetsService.compare(a.won, b.won, isAsc);
      case 'draw': return this.applicationAssetsService.compare(a.draw, b.draw, isAsc);
      case 'lost': return this.applicationAssetsService.compare(a.lost, b.lost, isAsc);
      case 'goalsInFavor': return this.applicationAssetsService.compare(a.goalsFor, b.goalsFor, isAsc);
      case 'goalsAgainst': return this.applicationAssetsService.compare(a.goalsAgainst, b.goalsAgainst, isAsc);
      case 'goalDifference': return this.applicationAssetsService.compare(a.goalDifference, b.goalDifference, isAsc);
      default: return 0;
    }
    });
  }

  ngOnDestroy(){
    this.onLoadNewCompetitionsDetailsSubscription.unsubscribe();
    this.onCompetitionsDetailsRenderedSubscription.unsubscribe();
  }
}