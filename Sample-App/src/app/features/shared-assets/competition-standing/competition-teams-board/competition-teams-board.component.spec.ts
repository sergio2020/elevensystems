import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitionTeamsBoardComponent } from './competition-teams-board.component';

describe('CompetitionTeamsBoardComponent', () => {
  let component: CompetitionTeamsBoardComponent;
  let fixture: ComponentFixture<CompetitionTeamsBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompetitionTeamsBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitionTeamsBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
