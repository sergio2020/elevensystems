import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompetitionTeamsBoardComponent } from 'src/app/features/shared-assets/competition-standing/competition-teams-board/competition-teams-board.component';
import { MatDialogModule, MatSortModule, MatTableModule } from '@angular/material';
import { TeamDetailDialogComponent } from './team/team-detail-dialog/team-detail-dialog.component';
import { TeamDetailComponent } from './team/team-detail/team-detail.component';
import { TeamPlayerListComponent } from './player/team-player-list/team-player-list.component';
import { TeamMatchesDialogComponent } from './match/team-matches-dialog/team-matches-dialog.component';
import { TeamMatchesListComponent } from './match/team-matches-list/team-matches-list.component';


@NgModule({
  declarations: [
    CompetitionTeamsBoardComponent,
    TeamDetailDialogComponent,
    TeamDetailComponent,
    TeamPlayerListComponent,
    TeamMatchesListComponent,
    TeamMatchesDialogComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatTableModule,
    MatSortModule,
    MatDialogModule
  ],
  exports:[
    CompetitionTeamsBoardComponent,
    TeamDetailDialogComponent,
    TeamDetailComponent,
    TeamPlayerListComponent
  ]
})
export class SharedAssetsModule { }