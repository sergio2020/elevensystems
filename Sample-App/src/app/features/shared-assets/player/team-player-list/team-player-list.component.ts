import { Component, Input, OnInit } from '@angular/core';
import { Player } from '../../../../models/player-model';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
  selector: 'app-team-player-list',
  templateUrl: './team-player-list.component.html',
  styleUrls: ['./team-player-list.component.scss']
})
export class TeamPlayerListComponent implements OnInit {
  @Input('squad') squad: Player[];
  displayedColumns: string[] = ['name','position','nationality','age'];
  loadingData:boolean = true;

  constructor() { }

  ngOnInit() {
    this.loadingData = false;
  }

  getPlayerAge(birthDate:string)
  {
    let parsedBirthDate = moment(birthDate);
    let today = moment();
    return today.diff(parsedBirthDate, 'years');
  }

}
