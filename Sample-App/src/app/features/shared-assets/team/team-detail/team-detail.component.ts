import { Component, Input, OnInit } from '@angular/core';
import { CompetitionArea } from '../../../../models/competition-area-model';
import { TeamApiService } from 'src/app/middleware/api-services/team-api.service';
import { Team } from '../../../../models/team-model';
import { TeamManagementService } from 'src/app/middleware/local-services/team-management.service';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.scss']
})
export class TeamDetailComponent implements OnInit {
  @Input('teamId') teamId: number;
  
  team:Team = new Team();
  loadingData:boolean = true;

  constructor(private teamApiService: TeamApiService, private teamManagementService:TeamManagementService) { }

  ngOnInit() {
    this.team.area = new CompetitionArea();
    this.loadTeamDetails();
  }

  private loadTeamDetails(){
    this.teamApiService.getTeamById(this.teamId).subscribe(team => {
      this.loadTeamDetailsCallback(team);
    });
  }

  private loadTeamDetailsCallback(team:Team){
    this.team = team;
    this.team.isFavorite = this.teamManagementService.checkIfTeamIsFavorite(this.team);
    this.loadingData = false;
  }

  addToFavorites(){
    this.teamManagementService.addTeamToFavorites(this.team);
  }

  deleteFromFavorites(){
    this.teamManagementService.deleteTeamFromFavorites(this.team);
  }
}
