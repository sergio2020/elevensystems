import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Team } from '../../../../models/team-model';

@Component({
  selector: 'app-team-detail-dialog',
  templateUrl: './team-detail-dialog.component.html',
  styleUrls: ['./team-detail-dialog.component.scss']
})
export class TeamDetailDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public team: Team, public dialogReference: MatDialogRef<TeamDetailDialogComponent>) { }

  ngOnInit() {
  }

  closeDialog(){
    this.dialogReference.close();
  }
}
