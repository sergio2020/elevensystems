import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamMatchesListComponent } from './team-matches-list.component';

describe('TeamMatchesListComponent', () => {
  let component: TeamMatchesListComponent;
  let fixture: ComponentFixture<TeamMatchesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamMatchesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamMatchesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
