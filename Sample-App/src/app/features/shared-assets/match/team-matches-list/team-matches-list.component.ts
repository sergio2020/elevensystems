import { Component, Input, OnInit } from '@angular/core';
import { Competition } from 'src/app/models/competition-model';
import { TeamApiService } from 'src/app/middleware/api-services/team-api.service';
import { Match } from '../../../../models/match-model';

@Component({
  selector: 'app-team-matches-list',
  templateUrl: './team-matches-list.component.html',
  styleUrls: ['./team-matches-list.component.scss']
})
export class TeamMatchesListComponent implements OnInit {
  @Input('teamId') teamId: number;
  availableCompetitions:Competition[] = new Array<Competition>();
  groupedMatches:any;
  displayedColumns: string[] = ['homeTeamName','awayTeamName','score','matchDate'];
  loadingData:boolean = true;

  constructor(private teamApiService:TeamApiService) { }

  ngOnInit() {
    this.loadMatchesData();
  }

  private loadMatchesData(){
    this.teamApiService.getTeamMatches(this.teamId).subscribe(matchCall => {
      this.loadMatchesDataCallback(matchCall.matches);
    });
  }

  private loadMatchesDataCallback(matches:Match[]){
    matches.forEach(match => {
      if(!this.availableCompetitions.some(comptetition => comptetition.id == match.competition.id))
      {
        this.availableCompetitions.push(match.competition);
      }
    });
    this.groupedMatches = matches.reduce((g : any, match : Match) => {
        g[match.competition.id] = g[match.competition.id] || [];
        g[match.competition.id].push(match);
        return g;
    }, {});
    this.loadingData = false;
  }
}
