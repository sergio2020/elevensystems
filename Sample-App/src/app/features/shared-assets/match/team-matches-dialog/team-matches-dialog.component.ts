import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Team } from '../../../../models/team-model';

@Component({
  selector: 'app-team-matches-dialog',
  templateUrl: './team-matches-dialog.component.html',
  styleUrls: ['./team-matches-dialog.component.scss']
})
export class TeamMatchesDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public team: Team, public dialogReference: MatDialogRef<TeamMatchesDialogComponent>) { }

  ngOnInit() {
  }
  
  closeDialog(){
    this.dialogReference.close();
  }
}
