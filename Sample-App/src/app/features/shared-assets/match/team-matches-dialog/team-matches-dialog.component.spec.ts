import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamMatchesDialogComponent } from './team-matches-dialog.component';

describe('TeamMatchesDialogComponent', () => {
  let component: TeamMatchesDialogComponent;
  let fixture: ComponentFixture<TeamMatchesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamMatchesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamMatchesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
