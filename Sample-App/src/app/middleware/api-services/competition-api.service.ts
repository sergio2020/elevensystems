import { Injectable } from '@angular/core';
import { SecuredApiService } from 'src/app/middleware/local-services/secured-api.service';
import { map } from 'rxjs/operators';
import { CompetitionApiContainer, CompetitionsApiContainer } from 'src/app/models/competition-model';
import { CompetitionStandingsApiContainer } from 'src/app/models/competition-standing-model';

@Injectable({
  providedIn: 'root'
})
export class CompetitionApiService {
  readonly serviceEndpoint:string = "competitions";

  constructor(private securedApi: SecuredApiService) { }

  getAllCompetitions() {
    return this.securedApi.securedHttpGetRequest(this.serviceEndpoint).pipe(
      map(value => Object.assign(new CompetitionsApiContainer(), value))
    );
  }

  getCompetitionTeamsCurrentSeason(competitionId:number) {
    return this.securedApi.securedHttpGetRequest(this.serviceEndpoint + "/" + competitionId + "/teams").pipe(
      map(value => Object.assign(new CompetitionApiContainer(), value))
    );
  }

  getCompetitionStandingsCurrentSeason(competitionId:number) {
    return this.securedApi.securedHttpGetRequest(this.serviceEndpoint + "/" + competitionId + "/standings").pipe(
      map(value => Object.assign(new CompetitionStandingsApiContainer(), value))
    );
  }
}
