import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { SecuredApiService } from 'src/app/middleware/local-services/secured-api.service';
import { MatchApiContainer } from 'src/app/models/match-model';
import { Team } from 'src/app/models/team-model';

@Injectable({
  providedIn: 'root'
})
export class TeamApiService {
  readonly serviceEndpoint:string = "teams";


  constructor(private securedApi: SecuredApiService) { }

  getTeamById(teamId:number){
    return this.securedApi.securedHttpGetRequest(this.serviceEndpoint + "/"  + teamId).pipe(
      map(value => Object.assign(new Team(), value))
    );
  }

  getTeamsByCompetitionCurrentSeason(competitionId:number) {
    return this.securedApi.securedHttpGetRequest(competitionId + "/" + this.serviceEndpoint).pipe(
      map(value => Object.assign(new Array<Team>(), value))
    );
  }

  getTeamMatches(teamId:number) {
    return this.securedApi.securedHttpGetRequest(this.serviceEndpoint + "/" + teamId + "/matches").pipe(
      map(value => Object.assign(new MatchApiContainer(), value))
    );
  }
}
