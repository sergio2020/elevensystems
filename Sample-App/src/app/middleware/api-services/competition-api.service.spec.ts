import { TestBed } from '@angular/core/testing';

import { CompetitionApiService } from './competition-api.service';

describe('CompetitionApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompetitionApiService = TestBed.get(CompetitionApiService);
    expect(service).toBeTruthy();
  });
});
