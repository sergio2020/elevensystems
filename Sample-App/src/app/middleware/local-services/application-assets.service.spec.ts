import { TestBed } from '@angular/core/testing';

import { ApplicationAssetsService } from './application-assets.service';

describe('ApplicationAssetsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationAssetsService = TestBed.get(ApplicationAssetsService);
    expect(service).toBeTruthy();
  });
});
