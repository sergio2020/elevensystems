import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApplicationAssetsService {

  constructor() { }
  
  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  compareDate(a:Date, b:Date, sortAscending:boolean):number{
    if(sortAscending){
      if(Number(a) < Number(b)) { return -1; }
      if(Number(a) > Number(b)) { return 1; }
      return 0;
    }
    else{
      if(Number(a) < Number(b)) { return 1; }
      if(Number(a) > Number(b)) { return -1; }
      return 0;
    }
  }
}
