import { Injectable } from '@angular/core';
import { Team } from 'src/app/models/team-model';

@Injectable({
  providedIn: 'root'
})
export class TeamManagementService {

  constructor() { }

  checkIfTeamIsFavorite(team:Team){
    if(localStorage.getItem('favoriteTeams') != null)
    {
      let favoriteTeams: Team[] = JSON.parse(localStorage.getItem('favoriteTeams'));
      return favoriteTeams.some(storedTeam => storedTeam.id == team.id);
    }
    else{
      return false;
    }
  }

  addTeamToFavorites(team:Team){
    let favoriteTeams: Team[] = new Array<Team>();
    if(localStorage.getItem('favoriteTeams') != null)
    {
      favoriteTeams = JSON.parse(localStorage.getItem('favoriteTeams'));
    }    
    favoriteTeams.push(team);
    localStorage.setItem('favoriteTeams', JSON.stringify(favoriteTeams));
    team.isFavorite = true;
  }

  deleteTeamFromFavorites(team:Team){
    let favoriteTeams: Team[] = JSON.parse(localStorage.getItem('favoriteTeams'));
    favoriteTeams = favoriteTeams.filter(storedTeam => storedTeam.id != team.id);
    localStorage.setItem('favoriteTeams', JSON.stringify(favoriteTeams));
    team.isFavorite = false;
  }
}
