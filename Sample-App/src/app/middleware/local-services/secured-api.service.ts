import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as app from '../../app.globals';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SecuredApiService {
  private baseUrl: string;
  private options: any;
  
  constructor(private http: HttpClient) {
    this.baseUrl = app.API_BASEURL; 
    this.options = {
      headers: new HttpHeaders({
        'X-Auth-Token': 'd9b2c29baac94818a4908116a55d6f08'
      })
    };   
   }

   securedHttpGetRequest(servicePath: string, callArguments: string = null){ 
    if(callArguments == null){
      return this.http.get<Observable<any>>(this.baseUrl + servicePath, this.options);
    }
    else{
      return this.http.get<Observable<any>>(this.baseUrl + servicePath + '?' + callArguments, this.options);
    }     
  }
}
