import { TestBed } from '@angular/core/testing';

import { SecuredApiService } from './secured-api.service';

describe('SecuredApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SecuredApiService = TestBed.get(SecuredApiService);
    expect(service).toBeTruthy();
  });
});
