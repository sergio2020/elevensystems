export class Season {
    id: number;
    startDate: Date;
    endDate:Date;
    currentMatchday:number;
}