import { Competition } from 'src/app/models/competition-model';
import { Score } from './score.model';
import { Season } from './season-model';
import { Team } from './team-model';

export class MatchApiContainer {
    count: number;
    filters: string;
    matches:Match[];
}

export class Match {
    id: number;
    competition: Competition;
    season: Season;
    utcDate: Date;
    status: string;
    matchday: number;
    stage: string;
    group: string;
    lastUpdated: Date;
    score:Score;
    homeTeam:Team;
    awayTeam:Team;
}