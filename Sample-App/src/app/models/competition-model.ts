import { CompetitionArea } from 'src/app/models/competition-area-model';
import { CompetitionStanding } from 'src/app/models/competition-standing-model';
import { Season } from 'src/app/models/season-model';
import { Team } from 'src/app/models/team-model';

export class CompetitionsApiContainer {
    count: number;
    filters: string;
    competitions: Competition[];
}

export class CompetitionApiContainer {
    count: number;
    filters: string;
    competition: Competition;
    season:Season;
    teams:Team[];
}

export class Competition {
    id: number;
    area: CompetitionArea;
    name: string;
    code: string; 
    emblemUrl: string;
    plan: string;
    numberOfAvailableSeasons:number;
    currentSeason:Season;
    seasons:Season[];
    lastUpdated:Date;
    isExpanded:boolean;
    teams:Team[];
    standings:CompetitionStanding[];
}