import { CompetitionArea } from './competition-area-model';
import { Player } from './player-model';

export class Team {
    id: number;
    area: CompetitionArea;
    name: string;
    shortName: string;
    tla: string;
    address: string;
    phone: string;
    website: string;
    email: string;
    founded: number;
    clubColors: string;
    venue: string;
    squad: Player[];
    isFavorite:boolean;
}