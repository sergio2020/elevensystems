import { Competition } from 'src/app/models/competition-model';
import { Season } from 'src/app/models/season-model';
import { Team } from 'src/app/models/team-model';

export class CompetitionStandingsApiContainer {
    filters: string;
    competition: Competition;
    season:Season;
    standings:CompetitionStanding[];
}

export class CompetitionStanding {
    stage: string;
    type: string;
    group: string;
    table: Standing[];
}

export class Standing {
    position: number;
    team: Team;
    playedGames: number;
    form: string;
    won: number;
    draw: number;
    lost: number;
    points: number;
    goalsFor: number;
    goalsAgainst: number;
    goalDifference: number;
}