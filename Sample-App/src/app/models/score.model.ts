export class Score {
    winner: string;
    duration: string;
    fullTime: ScoreDetail;
}

export class ScoreDetail{
    homeTeam: number;
    awayTeam: number;
}