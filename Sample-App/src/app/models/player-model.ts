export class Player {
    id: number;
    name: string;
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
    countryOfBirth: string;
    nationality: string;
    position: string;
    lastUpdated: Date;
}