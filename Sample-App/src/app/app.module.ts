import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { TeamDetailDialogComponent } from 'src/app/features/shared-assets/team/team-detail-dialog/team-detail-dialog.component';
import { TeamMatchesDialogComponent } from 'src/app/features/shared-assets/match/team-matches-dialog/team-matches-dialog.component';
import { DashboardModule } from './features/dashboard/dashboard.module';
import { FavoritesModule } from './features/favorites/favorites.module';
import { CompetitionDetailsModule } from './features/competition-details/competition-details.module';

import { StoreModule } from '@ngrx/store';
import { appReducer } from './redux/app.reducer';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [
    AppComponent    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    DashboardModule,
    FavoritesModule,
    CompetitionDetailsModule,
    StoreModule.forRoot({ count: appReducer }),
    EffectsModule.forRoot()
  ],
  entryComponents: [
    TeamDetailDialogComponent,
    TeamMatchesDialogComponent
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
